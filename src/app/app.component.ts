import { Component } from '@angular/core';
import { TitleService } from './services/title.service';
import { NbSidebarService } from '@nebular/theme';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
  private _title : string;
  public get title() : string {
    return this._title;
  }
  public set title(v : string) {
    this._title = v;
  }
  

  constructor(
    private titleService: TitleService,
    private sidebarService: NbSidebarService
  ) {
    this.titleService.titleSubject.subscribe(t => {
      this.title = t;
    });
  }

  open() {
    this.sidebarService.expand();
  }

  close(){
    this.sidebarService.toggle(true);
  }
}
