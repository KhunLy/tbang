import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExRoutingModule } from './ex-routing.module';
import { ExComponent } from './ex.component';
import { Ex1Component } from './components/ex1/ex1.component';
import { NbLayoutModule, NbButtonModule, NbListModule, NbInputModule, NbIconModule, NbSelectModule, NbCardModule, NbDialogModule, NbUserModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { Ex2Component } from './components/ex2/ex2.component';
import { FormsModule } from '@angular/forms';
import { SharedModuleModule } from '../shared-module/shared-module.module';
import { Ex3Component } from './components/ex3/ex3.component';
import { MasterComponent } from './components/ex3/master/master.component';
import { DetailsComponent } from './components/ex3/details/details.component';
import { ChampionProviderService } from './services/champion-provider.service';
import { ChampionImagePipe } from './pipes/champion-image.pipe';


@NgModule({
  declarations: [ExComponent, Ex1Component, Ex2Component, Ex3Component, MasterComponent, DetailsComponent, ChampionImagePipe ],
  imports: [
    CommonModule,
    ExRoutingModule,
    NbLayoutModule,
    NbEvaIconsModule,
    NbButtonModule,
    NbListModule,
    NbInputModule,
    NbIconModule,
    NbSelectModule,
    NbCardModule,
    NbUserModule,
    FormsModule,
    NbDialogModule.forChild(),
    SharedModuleModule,
  ],
  providers: [ChampionProviderService]
})
export class ExModule { }
