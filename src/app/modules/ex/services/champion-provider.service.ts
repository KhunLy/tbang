import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ChampionsRequest } from '../models/champions-request';

@Injectable()
export class ChampionProviderService {
  private readonly _endpointAll: string = "http://ddragon.leagueoflegends.com/cdn/6.24.1/data/en_US/champion.json";
  private readonly _endpointByName: string = "http://ddragon.leagueoflegends.com/cdn/6.24.1/data/en_US/champion/__name__.json";

  constructor(
    private httpClient: HttpClient
  ) { }

  getAll(): Observable<ChampionsRequest> {
    return this.httpClient.get<ChampionsRequest>(this._endpointAll);
  }

  getByName(name: string): Observable<any> {
    return this.httpClient.get(this._endpointByName.replace('__name__', name));
  }
}
