import { TestBed } from '@angular/core/testing';

import { ChampionProviderService } from './champion-provider.service';

describe('ChampionProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ChampionProviderService = TestBed.get(ChampionProviderService);
    expect(service).toBeTruthy();
  });
});
