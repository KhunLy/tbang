import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ChampionsRequest } from '../../../models/champions-request';
import { ChampionProviderService } from '../../../services/champion-provider.service';

@Component({
  selector: 'app-master',
  templateUrl: './master.component.html',
  styleUrls: ['./master.component.scss']
})
export class MasterComponent implements OnInit {

  
  private _model : ChampionsRequest;
  public get model() : ChampionsRequest {
    return this._model;
  }
  public set model(v : ChampionsRequest) {
    this._model = v;
  }

  
  private _select : EventEmitter<string>;
  @Output() public get select() : EventEmitter<string> {
    return this._select;
  }
  public set select(v : EventEmitter<string>) {
    this._select = v;
  }
  
  

  constructor(
    private championProvider: ChampionProviderService
  ) {
    this.select = new EventEmitter<string>();
  }

  ngOnInit() {
    this.championProvider.getAll().subscribe(data => {
      this.model = data;
    });
  }

  onSelect(name: string) {
    this.select.emit(name);
  }

}
