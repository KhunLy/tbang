import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  
  private _name : string;
  public get name() : string {
    return this._name;
  }
  @Input() public set name(v : string) {
    this._name = v;
  }
  

  constructor() { }

  ngOnInit() {
  }

}
