import { Component, OnInit } from '@angular/core';
import { NbDialogService, NbDialogModule } from '@nebular/theme';
import { ConfirmDialogComponent } from 'src/app/modules/shared-module/components/confirm-dialog/confirm-dialog.component';
import { Article } from '../../models/article';

@Component({
  selector: 'app-ex2',
  templateUrl: './ex2.component.html',
  styleUrls: ['./ex2.component.scss']
})
export class Ex2Component implements OnInit {

  
  private _input : string;
  public get input() : string {
    return this._input;
  }
  public set input(v : string) {
    this._input = v;
  }

  
  private _items : Article[];
  public get items() : Article[] {
    return this._items;
  }
  public set items(v : Article[]) {
    this._items = v;
  }
  
  

  constructor(private nbDialServ: NbDialogService) { }

  ngOnInit() {
    this.items = [];
  }

  add() {
    let article = this.items
      .find(
        a => a.name.toLowerCase().trim() == this.input.toLowerCase().trim()
    );
    if(!article){
      this.items.push({
        name: this.input, 
        isChecked: false, 
        quantity: 1
      });
    }
    else{
      article.quantity++;
    }
    
    this.input = null;
  }

  delete(item: Article) {
    let index: number = this.items.indexOf(item);
    this.items.splice(index, 1);
  }

  openConfirmation(item: Article){
    let ref = this.nbDialServ.open(ConfirmDialogComponent, {
      context: { item: item.name }
    });
    // ouverture d'1 boite de confirmation
    ref.onClose.subscribe((data) => {
      if(data) {
        this.delete(item);
      }
    });
  }

  check(item: Article) {
    item.isChecked = !item.isChecked;
  }

}
