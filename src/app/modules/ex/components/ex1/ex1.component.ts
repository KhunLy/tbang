import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ex1',
  templateUrl: './ex1.component.html',
  styleUrls: ['./ex1.component.scss']
})
export class Ex1Component implements OnInit {

  
  
  private _value : number;
  public get value() : number {
    return this._value;
  }
  public set value(v : number) {
    this._value = v;
  }

  private interval: any;

  public get isStarted(): boolean{
    return this.interval != null;
  }
  
  

  constructor() { }

  ngOnInit() {
    this.value = 0;
  }

  start() {
    if(this.interval == null) {
      this.interval = setInterval(() => {
        this.value++;
      }, 1000);
    }
    
  }

  stop() {
    clearInterval(this.interval);
    this.interval = null;
  }

  reset() {
    this.value = 0;
    this.stop();
  }

}
