import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'championImage'
})
export class ChampionImagePipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    return `http://ddragon.leagueoflegends.com/cdn/6.24.1/img/champion/${value}`;
  }

}
