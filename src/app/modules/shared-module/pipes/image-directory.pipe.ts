import { Pipe, PipeTransform } from '@angular/core';
import { environment } from 'src/environments/environment';

@Pipe({
  name: 'img_dir'
})
export class ImageDirectoryPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    return environment.img_directory + value;
  }

}
