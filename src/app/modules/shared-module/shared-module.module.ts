import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmDialogComponent } from './components/confirm-dialog/confirm-dialog.component';
import { NbCardModule, NbButtonModule, NbIconModule, NbDialogModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { LoaderComponent } from './components/loader/loader.component';
import { AlertComponent } from './components/alert/alert.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoaderInterceptor } from './interceptors/loader.interceptor';
import { ImageDirectoryPipe } from './pipes/image-directory.pipe';
import { HighlightDirective } from './directives/highlight.directive';
import { TooltipDirective } from './directives/tooltip.directive';



@NgModule({
  declarations: [ConfirmDialogComponent, LoaderComponent, AlertComponent, ImageDirectoryPipe, HighlightDirective, TooltipDirective],
  imports: [
    CommonModule,
    NbCardModule,
    NbButtonModule,
    NbEvaIconsModule,
    NbIconModule,
    NbDialogModule.forChild(),
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true }
  ],
  entryComponents: [ ConfirmDialogComponent, LoaderComponent, AlertComponent ],
  exports: [ ConfirmDialogComponent, LoaderComponent, AlertComponent, ImageDirectoryPipe, HighlightDirective, TooltipDirective ]
})
export class SharedModuleModule { }
