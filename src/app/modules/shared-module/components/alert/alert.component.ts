import { Component, OnInit, Input, OnDestroy, ElementRef, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit, OnDestroy {

  
  private _close : EventEmitter<boolean>;
  @Output() public get close() : EventEmitter<boolean> {
    return this._close;
  }
  public set close(v : EventEmitter<boolean>) {
    this._close = v;
  }

  private _color : string;
  public get color() : string {
    return this._color;
  }
  @Input() public set color(v : string) {
    this._color = v;
  }
  
  constructor() {
    this.close = new EventEmitter<boolean>();
  }

  ngOnInit() {
    
  }

  ngOnDestroy() {
    console.log("this alert is destroyed");
  }

  dismiss() {
    //this.isDismissed = true;
    this.close.emit(true);
  }

}
