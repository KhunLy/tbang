import { Component, OnInit } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {

  constructor(private ref: NbDialogRef<LoaderComponent>) { }

  ngOnInit() {
  }

  cancel(){
    this.ref.close(true);
  }

}
