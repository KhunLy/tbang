import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor() { }

  saveToken(token: string){
    sessionStorage.setItem('TOKEN', token)
  }

  clearSession() {
    sessionStorage.clear();
  }

  getToken(): string {
    return sessionStorage.getItem('TOKEN');
  }


}
