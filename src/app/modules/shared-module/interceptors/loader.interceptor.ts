import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NbDialogService } from '@nebular/theme';
import { LoaderComponent } from '../components/loader/loader.component';
import { finalize } from 'rxjs/operators';

@Injectable()
export class LoaderInterceptor implements HttpInterceptor {

  constructor(private dialogService: NbDialogService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler)
    : Observable<HttpEvent<any>> {
    let ref = this.dialogService.open(LoaderComponent);
    // req.headers.append()
    return next.handle(req).pipe(finalize(() => {
      setTimeout(() => {
        ref.close();
      }, 500)
    }));

  }

}
