import { Directive, ElementRef, Inject, HostListener, Input } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Directive({
  selector: '[appTooltip]'
})
export class TooltipDirective {

  @HostListener("mouseover") onMouseOver() {
    this.div.style.display = "block";
    this.div.innerText = this._appTooltip;
  }

  @HostListener("mouseleave") onMouseLeave() {
    this.div.style.display = "none";
  }

  
  private _appTooltip : string;
  @Input() public set appTooltip(v : string) {
    this._appTooltip = v;
  }
  

  
  private _div : any;
  public get div() : any {
    return this._div;
  }
  public set div(v : any) {
    this._div = v;
  }
  

  constructor(
    private ref: ElementRef,
    @Inject(DOCUMENT) private doc: Document
  ) {
    this.div = doc.createElement("div");
    
    this.div.style.border = "1px solid black";
    this.div.style.borderRadius = "5px";
    this.div.style.backgroundColor = "white";
    this.div.style.padding = "5px";
    this.div.style.position = "absolute";
    this.div.style.bottom = "20px";
    this.div.style.display = "none";
    ref.nativeElement.appendChild(this.div);
    ref.nativeElement.style.position = "relative";
  }

}
