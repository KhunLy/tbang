import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {

  @HostListener('mouseover') onMouseOver() {
    this.ref.nativeElement.style.backgroundColor = this._appHighlight || 'yellow';
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.ref.nativeElement.style.backgroundColor = 'transparent';
  }

  private _appHighlight: string;

  @Input() public set appHighlight(v: string) {
    this._appHighlight = v;
  }

  constructor(
    private ref: ElementRef
  ) { 
    
  }

}
