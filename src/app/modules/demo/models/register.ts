export interface Register {
    email: string;
    lastName: string;
    firstName: string;
    password: string;
    birthDate?: Date;
    condAgreement: boolean;
}