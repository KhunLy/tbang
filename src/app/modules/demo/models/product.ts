export interface Product {
    name: string;
    price: number;
    discount: number;
    image: string;
    priceWithDiscount?: () => number;
}
