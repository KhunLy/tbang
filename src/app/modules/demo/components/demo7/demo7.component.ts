import { Component, OnInit } from '@angular/core';
import { Product } from '../../models/product';

@Component({
  selector: 'app-demo7',
  templateUrl: './demo7.component.html',
  styleUrls: ['./demo7.component.scss']
})
export class Demo7Component implements OnInit {

  
  private _collection : Product[];
  public get collection() : Product[] {
    return this._collection;
  }
  public set collection(v : Product[]) {
    this._collection = v;
  }
  

  constructor() { }

  ngOnInit() {
    this.collection = [
      { name: "Coca Cola", discount: 10, image: "coca.jpg", price: 0.7 },
      { name: "Sprite", discount: null, image: "sprite.jpg", price: 0.6 },
      { name: "Fanta", discount: 20, image: "fanta.jpg", price: 0.6 },
      { name: "Red Bull", discount: null, image: "red-bull.jpg", price: 2 },
      { name: "Nalu", discount: 15, image: "nalu.jpg", price: 1.3 },
    ];
    this.collection.forEach(i => i.priceWithDiscount = () => {
      return i.price - (i.price * i.discount / 100);
    })
  }

}
