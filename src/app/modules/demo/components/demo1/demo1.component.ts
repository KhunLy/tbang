import { Component, OnInit } from '@angular/core';
import { TitleService } from 'src/app/services/title.service';

@Component({
  selector: 'app-demo1',
  templateUrl: './demo1.component.html',
  styleUrls: ['./demo1.component.scss']
})
export class Demo1Component implements OnInit {

  
  private _maProp : string;
  public get maProp() : string {
    return this._maProp;
  }
  public set maProp(v : string) {
    this._maProp = v;
  }
  
  constructor(
    private titleService: TitleService
  ) { }

  ngOnInit() {
    this.titleService.title = 'Demo 1 - Binding 1 Way';
    this.maProp = "World";
    setTimeout(() => {
      this.maProp = "Khun";
    } , 3000);
  }

}
