import { Component, OnInit } from '@angular/core';
import { TitleService } from 'src/app/services/title.service';
import { WeatherProviderService } from '../../services/weather-provider.service';
import { WeatherRequest } from '../../models/weather-request';

@Component({
  selector: 'app-demo9',
  templateUrl: './demo9.component.html',
  styleUrls: ['./demo9.component.scss']
})
export class Demo9Component implements OnInit {

  
  private _model : WeatherRequest;
  public get model() : WeatherRequest {
    return this._model;
  }
  public set model(v : WeatherRequest) {
    this._model = v;
  }
  
  constructor(
    private titleService: TitleService,
    private weatherProvider: WeatherProviderService
  ) { }

  ngOnInit() {
    this.titleService.title = "Demo 9 - HttpClient";
    this.weatherProvider.getWeather(85, -3.7).subscribe((data) => {
      this.model = data;
    }, null, null);
  }

}
