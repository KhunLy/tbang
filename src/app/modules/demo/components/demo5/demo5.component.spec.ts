import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Demo5Component } from './demo5.component';
import { FormsModule } from '@angular/forms';

describe('Demo5Component', () => {
  let component: Demo5Component;
  let fixture: ComponentFixture<Demo5Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [ Demo5Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Demo5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
