import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo5',
  templateUrl: './demo5.component.html',
  styleUrls: ['./demo5.component.scss']
})
export class Demo5Component implements OnInit {

  
  private _input : string;
  public get input() : string {
    return this._input;
  }
  public set input(v : string) {
    this._input = v;
  }
  
  constructor() { }

  ngOnInit() {
    this.input = 'Khun';
  }

}
