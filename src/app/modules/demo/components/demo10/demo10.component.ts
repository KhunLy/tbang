import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Register } from '../../models/register';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-demo10',
  templateUrl: './demo10.component.html',
  styleUrls: ['./demo10.component.scss']
})
export class Demo10Component implements OnInit {

  
  private _formGroup : FormGroup;
  public get formGroup() : FormGroup {
    return this._formGroup;
  }
  public set formGroup(v : FormGroup) {
    this._formGroup = v;
  }
  

  constructor(private client: HttpClient) {
    
   }

  ngOnInit() {
    this.formGroup = new FormGroup({
      'email' : new FormControl(null, Validators.compose(
        [Validators.email, Validators.required]
      )),
      'lastName' : new FormControl(null, Validators.compose([
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(25)
      ])),
      'firstName' : new FormControl(null, Validators.compose([
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(25)
      ])),
      'password' : new FormControl(null, Validators.compose([
        Validators.required,
        Validators.pattern(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}$/)
      ])),
      'confirmPassword' : new FormControl(null, Validators.compose([
        Validators.required,
      ])),
      'birthDate': new FormControl(new Date(), Validators.compose([
        this.birthDateValidator
      ])),
      'condAgreement': new FormControl(false, Validators.compose([
        Validators.requiredTrue
      ]))
    }, Validators.compose([
      this.comparePwd
    ]));
    
  }

  comparePwd(group: FormGroup) {
    let pwd = group.get('password').value;
    let c_pwd = group.get('confirmPassword').value;
    return c_pwd == pwd ? null : { notSame : true };
  }

  birthDateValidator(control: FormControl) {
    return control.value.getTime() >= Date.now() ? { notValid: true } : null
  }

  onSubmit(value: Register){
    if(this.formGroup.valid){
    //envoyer l'objet vers une api
      this.client.post(
        "https://testapi-883bb.firebaseio.com/users.json", value
      ).subscribe((data) => console.log(data));
    }
  }

}
