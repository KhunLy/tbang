import { Component, OnInit, ViewChild, ViewContainerRef, ComponentFactoryResolver, Injector, Inject } from '@angular/core';
import { AlertComponent } from 'src/app/modules/shared-module/components/alert/alert.component';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-demo8',
  templateUrl: './demo8.component.html',
  styleUrls: ['./demo8.component.scss']
})
export class Demo8Component implements OnInit {

  @ViewChild(
    "container", 
    {read: ViewContainerRef, static: true}
  ) container: ViewContainerRef; 

  
  private _isClosed : boolean;
  public get isClosed() : boolean {
    return this._isClosed;
  }
  public set isClosed(v : boolean) {
    this._isClosed = v;
  }

  
  private _appAlertDismissed : boolean;
  public get appAlertDismissed() : boolean {
    return this._appAlertDismissed;
  }
  public set appAlertDismissed(v : boolean) {
    this._appAlertDismissed = v;
  }
  
  

  constructor(
    private resolver: ComponentFactoryResolver,
    @Inject(DOCUMENT) private document: Document
  ) { }

  ngOnInit() {
  }
  // facultatif
  addAlert() {
    let alertFactory 
      = this.resolver.resolveComponentFactory(AlertComponent);
    let h1 = this.document.createElement("h1");
    h1.innerText = "Alert Title";
    let h4 = this.document.createElement("h4");
    h4.innerText = "Alert Message"
    let componentRef = this.container.createComponent(
      alertFactory,
      this.container.length,
      null,
      [[h1, h4]]
    );
    componentRef.instance.color = "danger";
    componentRef.instance.close.subscribe((data) => {
      componentRef.destroy();
      console.log(data);
    })
  }
  //end facultatif

  onCloseAppAlert() {
    // on supprimera l'élément app-alert
    this.appAlertDismissed = true;
  }

}
