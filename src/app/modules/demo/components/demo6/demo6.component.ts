import { Component, OnInit } from '@angular/core';
import { Observable, observable } from 'rxjs';
import { NbDialogService } from '@nebular/theme';
import { LoaderComponent } from 'src/app/modules/shared-module/components/loader/loader.component';

@Component({
  selector: 'app-demo6',
  templateUrl: './demo6.component.html',
  styleUrls: ['./demo6.component.scss']
})
export class Demo6Component implements OnInit {

  private _status : string;
  public get status() : string {
    return this._status;
  }
  public set status(v : string) {
    this._status = v;
  }

  private _response : number;
  public get response() : number {
    return this._response;
  }
  public set response(v : number) {
    this._response = v;
  }
  
  constructor(private dialServ: NbDialogService) { }

  ngOnInit() {
    // simulation d'une requête http;
    let fakeHttpRequest = new Observable<number>((observer) => {
      setTimeout(() => {
        observer.next(42);
        observer.complete();
      } , 5000);
    });

    let dialogRef = this.dialServ.open(LoaderComponent, { 
      closeOnBackdropClick: false 
    });

    let httpSubscription = fakeHttpRequest.subscribe(
      // on Response
      data => {
        this.response = data;
      },
      // on Error
      error => {

      },
      //on Complete
      () => {
        this.status = "COMPLETE";
        dialogRef.close();
      }
    );

    this.status = "LOADING ...";
    dialogRef.onClose.subscribe(data => {
      if(data) {
        this.status = "CANCELLED";
        httpSubscription.unsubscribe();
      }
    });
  }

}
