import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo3',
  templateUrl: './demo3.component.html',
  styleUrls: ['./demo3.component.scss']
})
export class Demo3Component implements OnInit {

  
  private _isToggled : boolean;
  public get isToggled() : boolean {
    return this._isToggled;
  }
  public set isToggled(v : boolean) {
    this._isToggled = v;
  }
  

  constructor() { }

  toggle() {
    this.isToggled = !this.isToggled;
  }

  ngOnInit() {
  }

}
