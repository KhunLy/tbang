import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo4',
  templateUrl: './demo4.component.html',
  styleUrls: ['./demo4.component.scss']
})
export class Demo4Component implements OnInit {

  
  private _collection : string[];
  public get collection() : string[] {
    return this._collection;
  }
  public set collection(v : string[]) {
    this._collection = v;
  }
  

  constructor() { }

  ngOnInit() {
    this.collection = ["Pommes", "Poires", "Scoubidous"]
  }

}
