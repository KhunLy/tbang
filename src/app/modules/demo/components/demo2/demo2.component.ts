import { Component, OnInit } from '@angular/core';
import { TitleService } from 'src/app/services/title.service';

@Component({
  selector: 'app-demo2',
  templateUrl: './demo2.component.html',
  styleUrls: ['./demo2.component.scss']
})
export class Demo2Component implements OnInit {

  
  private _compt : number;
  public get compt() : number {
    return this._compt;
  }
  public set compt(v : number) {
    this._compt = v;
  }

  
  private _compt2 : number;
  public get compt2() : number {
    return this._compt2;
  }
  public set compt2(v : number) {
    this._compt2 = v;
  }
  
  
  constructor(
    private titleService: TitleService
  ) { }

  ngOnInit() {
    this.compt = 0;
    this.compt2 = 0;
    this.titleService.title = "Demo 2 - Events";
  }

  onMouseOver() {
    this.compt++;
  }

  onPlusClick() {
    this.compt2++;
  }

  onMinusClick() {
    this.compt2--;
  }

}
