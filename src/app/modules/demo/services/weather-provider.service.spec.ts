import { TestBed } from '@angular/core/testing';

import { WeatherProviderService } from './weather-provider.service';

describe('WeatherProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WeatherProviderService = TestBed.get(WeatherProviderService);
    expect(service).toBeTruthy();
  });
});
