import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { WeatherRequest } from '../models/weather-request';

@Injectable()
export class WeatherProviderService {

  private readonly _url: string 
    = "https://api.openweathermap.org/data/2.5/weather?units=metric&lat=__lat__&lon=__lng__&APPID=__apiKey__";

  constructor(
    // ATTENTION PAS CELUI DE SELENIUM
    private httpClient: HttpClient
  ) { }

  getWeather(lat: number, lng: number): Observable<WeatherRequest> {
    return this.httpClient.get<WeatherRequest>(this._url
      .replace("__lat__", lat.toString())
      .replace("__lng__", lng.toString())
      .replace("__apiKey__", environment.OWM_APIKEY)
    );
  }
}
