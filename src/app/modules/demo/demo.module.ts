import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoRoutingModule } from './demo-routing.module';
import { DemoComponent } from './demo.component';
import { NbLayoutModule, NbButtonModule, NbListModule, NbInputModule, NbIconModule, NbSelectModule, NbCardModule, NbDatepicker, NbDatepickerModule, NbCheckboxModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { Demo1Component } from './components/demo1/demo1.component';
import { Demo2Component } from './components/demo2/demo2.component';
import { Demo3Component } from './components/demo3/demo3.component';
import { Demo4Component } from './components/demo4/demo4.component';
import { Demo5Component } from './components/demo5/demo5.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModuleModule } from '../shared-module/shared-module.module';
import { Demo6Component } from './components/demo6/demo6.component';
import { Demo7Component } from './components/demo7/demo7.component';
import { Demo8Component } from './components/demo8/demo8.component';
import { Demo9Component } from './components/demo9/demo9.component';
import { WeatherProviderService } from './services/weather-provider.service';
import { Demo10Component } from './components/demo10/demo10.component';


@NgModule({
  declarations: [DemoComponent, Demo1Component, Demo2Component, Demo3Component, Demo4Component, Demo5Component, Demo6Component, Demo7Component, Demo8Component, Demo9Component, Demo10Component],
  imports: [
    CommonModule,
    DemoRoutingModule,
    NbLayoutModule,
    NbEvaIconsModule,
    NbButtonModule,
    NbListModule,
    NbInputModule,
    NbIconModule,
    NbSelectModule,
    NbCardModule,
    NbDatepickerModule,
    NbCheckboxModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModuleModule,
  ],
  providers: [
    WeatherProviderService
  ]
})
export class DemoModule { }
