import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './components/nav/nav.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbThemeModule, NbLayoutModule, NbListModule, NbInputModule, NbIconModule, NbSelectModule, NbSidebarModule, NbCardModule, NbMenuModule, NbButtonModule, NbDialogModule, NbActionsModule, NbDatepickerModule, NbTooltipModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { SharedModuleModule } from './modules/shared-module/shared-module.module';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './components/login/login.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    HomeComponent,
    AboutComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NbThemeModule.forRoot({ name: 'default' }),
    NbLayoutModule,
    NbEvaIconsModule,
    NbButtonModule,
    NbListModule,
    NbInputModule,
    NbIconModule,
    NbSelectModule,
    NbSidebarModule.forRoot(),
    NbCardModule,
    NbMenuModule.forRoot(),
    NbDialogModule.forRoot(),
    NbActionsModule,
    NbDatepickerModule.forRoot(),
    SharedModuleModule,
    HttpClientModule,
    NbTooltipModule,
    ReactiveFormsModule
  ],
  providers: [
    //TitleService,
    //{ provide: TitleServiceInterface, useClass: TitleService, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
