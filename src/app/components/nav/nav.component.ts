import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  
  
  private _items : any;
  public get items() : any {
    return this._items;
  }
  public set items(v : any) {
    this._items = v;
  }
  
  

  constructor() { 
    this.items = [
      { title: 'Home', link: '/home', icon: 'home' },
      { title: 'About', link: '/about', icon: 'star' },
      { title: 'Demo', icon: 'sun', children: [
        { title: 'Demo1 - Binding One Way', link: '/demo/demo1' },
        { title: 'Demo2 - Events', link: '/demo/demo2' },
        { title: 'Demo3 - *ngIf', link: '/demo/demo3' },
        { title: 'Demo4 - *ngFor', link: '/demo/demo4' },
        { title: 'Demo5 - Binding Two Ways', link: '/demo/demo5' },
        { title: 'Demo6 - Observable', link: '/demo/demo6' },
        { title: 'Demo7 - Models', link: '/demo/demo7' },
        { title: 'Demo8 - @Input() - @Output()', link: '/demo/demo8' },
        { title: 'Demo9 - HttpClient', link: '/demo/demo9' },
        { title: 'Demo10 - Reactive Forms', link: '/demo/demo10' },
      ] },
      { title: 'Exercices', icon: 'folder', children: [
        { title: 'Ex1 - Timer', link: '/ex/ex1' },
        { title: 'Ex2 - Shopping List', link: '/ex/ex2' },
        { title: 'Ex3 - API', link: '/ex/ex3' },
      ] }
    ]
  }

  ngOnInit() {

  }

}
