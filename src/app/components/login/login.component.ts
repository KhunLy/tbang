import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LoginService } from 'src/app/modules/shared-module/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  
  private _form : FormGroup;
  public get form() : FormGroup {
    return this._form;
  }
  public set form(v : FormGroup) {
    this._form = v;
  }
  
  constructor(private loginService: LoginService) { }

  ngOnInit() {
    this.form = new FormGroup({
      "email": new FormControl(null, Validators.compose([
        Validators.email, Validators.required
      ])),
      "password": new FormControl(null, Validators.compose([
        Validators.minLength(2), Validators.required
      ])),
    })

  }
  onSubmit() {
    this.loginService.login(
      this.form.value.email, this.form.value.password
    );
  }

}
