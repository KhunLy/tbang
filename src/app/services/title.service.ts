import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';

@Injectable({providedIn: 'root'})
export class TitleService {

  
  private _titleSubject : BehaviorSubject<string>;

  public get titleSubject() : BehaviorSubject<string> {
    return this._titleSubject;
  }
  public set title(v : string) {
    this._titleSubject.next(v);
  }
  

  constructor(
    //injection de dépendence par constructeur
    //private httpClient: HttpClient
  ) {
    this._titleSubject = new BehaviorSubject<string>("");
  }
}
